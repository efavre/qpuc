//
//  EFThirdViewController.m
//  qpuc
//
//  Created by Eric Favre on 11/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "EFThirdViewController.h"
#import "EFSoundManager.h"

#define INITIAL_LEFT_X 200
#define INITIAL_RIGHT_X 300
#define LEFT_RIGHT_OFFSET 100
#define PIXEL_TIME_RATIO 30

@interface EFThirdViewController ()
@property(nonatomic) BOOL player1Plays;
@property(nonatomic) BOOL gamePaused;
@property(nonatomic) NSUInteger currentBlockNumber;
@end

@implementation EFThirdViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [EFSoundManager sharedManager];
}

-(void)viewDidAppear:(BOOL)animated
{
    for (UIView *blockBackgroundView in @[self.block1BackgroundView, self.block2BackgroundView, self.block3BackgroundView, self.block4BackgroundView])
    {
        blockBackgroundView.layer.borderColor = [UIColor blackColor].CGColor;
        blockBackgroundView.layer.borderWidth = 1.0f;
    }
    [self startOver:nil];
}

#pragma mark - User Actions

- (IBAction)startTimer:(id)sender
{
    self.player1Button.enabled = YES;
    self.player2Button.enabled = YES;
    self.switchHandsButton.enabled = NO;
    self.startButton.enabled = NO;
    if (self.gamePaused)
    {
        self.gamePaused = NO;
        [self resumeLayer:[self getBlockViewForNumber:self.currentBlockNumber].layer];
    }
    else
    {
        [self animateBlock];
    }
}

- (IBAction)buzzerPushed:(id)sender
{
    [[EFSoundManager sharedManager] playBuzzerSound];
    
    self.player2Button.enabled = NO;
    self.player1Button.enabled = NO;
    
    self.gamePaused = YES;
    [self pauseLayer:[self getBlockViewForNumber:self.currentBlockNumber].layer];
    self.startButton.enabled = YES;
}

- (IBAction)startOver:(id)sender
{
    self.player1Button.enabled = NO;
    self.player2Button.enabled = NO;
    self.startOverButton.enabled = NO;
    self.switchHandsButton.enabled = YES;
    self.startButton.enabled = YES;
    self.player1Plays = YES;
    self.currentBlockNumber = 4;
    self.gamePaused = NO;
    [self resetBlocks];
}

- (IBAction)switchHand:(id)sender
{
    NSInteger offset = LEFT_RIGHT_OFFSET;
    if (self.player1Plays)
    {
        offset = -LEFT_RIGHT_OFFSET;
    }
    for (UIView *blockBackgroundView in @[self.block1BackgroundView, self.block2BackgroundView, self.block3BackgroundView, self.block4BackgroundView]) {
        blockBackgroundView.frame = CGRectMake(blockBackgroundView.frame.origin.x + offset, blockBackgroundView.frame.origin.y, blockBackgroundView.frame.size.width, blockBackgroundView.frame.size.height);
        offset = -offset;
    }
    self.player1Plays = !self.player1Plays;
}

- (IBAction)wrongAnswer:(id)sender
{
    UIView *currentView = [[self getBlockViewForNumber:self.currentBlockNumber] superview];
    UIView *nextView = nil;
    
    if (self.currentBlockNumber > 1)
    {
        nextView =[[self getBlockViewForNumber:(self.currentBlockNumber - 1)] superview];
    }
    
    NSUInteger currentViewXCenter = currentView.frame.origin.x + (currentView.frame.size.width/2);
    NSUInteger offset = 100;
    if (currentViewXCenter < self.view.frame.size.width / 2)
    {
        if (nextView && nextView.frame.origin.x == currentView.frame.origin.x) {
            nextView.frame = CGRectMake(nextView.frame.origin.x + offset, nextView.frame.origin.y, nextView.frame.size.width, nextView.frame.size.height);
        }
        currentView.frame = CGRectMake(currentView.frame.origin.x + offset, currentView.frame.origin.y, currentView.frame.size.width, currentView.frame.size.height);
    }
    else
    {
        if (nextView && nextView.frame.origin.x == currentView.frame.origin.x) {
            nextView.frame = CGRectMake(nextView.frame.origin.x - offset, nextView.frame.origin.y, nextView.frame.size.width, nextView.frame.size.height);
        }
        currentView.frame = CGRectMake(currentView.frame.origin.x - offset, currentView.frame.origin.y, currentView.frame.size.width, currentView.frame.size.height);
    }
}

#pragma mark - Utils

- (void)resetBlocks
{
    for (int tag = 1 ; tag <= 4 ; tag++) {
        UIView *blockBackgroundView = [self.view viewWithTag:tag];
        float xPosition = INITIAL_RIGHT_X;
        if (tag % 2 == 0)
        {
            xPosition = INITIAL_LEFT_X;
        }
        blockBackgroundView.frame = CGRectMake(xPosition, blockBackgroundView.frame.origin.y, blockBackgroundView.frame.size.width, blockBackgroundView.frame.size.height);
        UIView *blockView = [self getBlockViewForNumber:tag];
        blockView.frame = CGRectMake(0, 0, blockView.frame.size.width, blockBackgroundView.frame.size.height);
    }
}

- (UIView *)getBlockViewForNumber:(NSUInteger)blockNumber
{
    int tag = (int)blockNumber + 100;
    return [self.view viewWithTag:tag];
}

- (void)animateBlock
{
    UIView *currentBlockView = [self getBlockViewForNumber:self.currentBlockNumber];
    NSUInteger remainingTime = currentBlockView.frame.size.height / PIXEL_TIME_RATIO;
    CGRect newBlockFrame = currentBlockView.frame;
    newBlockFrame.origin.y = newBlockFrame.origin.y + newBlockFrame.size.height;
    newBlockFrame.size.height = 0;
    [UIView animateWithDuration:remainingTime delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        currentBlockView.frame = newBlockFrame;
    } completion:^(BOOL finished) {
        self.currentBlockNumber --;
        if (self.currentBlockNumber > 0)
        {
            [self animateBlock];
        }
        else
        {
            self.startOverButton.enabled = YES;
        }
    }];
}

#pragma mark - Layer actions

- (void)pauseLayer:(CALayer *)layer
{
	CFTimeInterval pausedTime = [layer convertTime:CACurrentMediaTime() fromLayer:nil];
	layer.speed = 0.0;
	layer.timeOffset = pausedTime;
}

- (void)resumeLayer:(CALayer *)layer
{
	CFTimeInterval pausedTime = [layer timeOffset];
	layer.speed = 1.0;
	layer.timeOffset = 0.0;
	layer.beginTime = 0.0;
	CFTimeInterval timeSincePause = [layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
	layer.beginTime = timeSincePause;
}

@end
