//
//  EFFirstViewController.h
//  qpuc
//
//  Created by Eric Favre on 11/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFFirstViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *user1Button;
@property (weak, nonatomic) IBOutlet UIButton *user2Button;
@property (weak, nonatomic) IBOutlet UIButton *user3Button;
@property (weak, nonatomic) IBOutlet UIButton *user4Button;

@end
