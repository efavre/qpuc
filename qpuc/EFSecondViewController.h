//
//  EFSecondViewController.h
//  qpuc
//
//  Created by Eric Favre on 11/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFSecondViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *answer1Label;
@property (weak, nonatomic) IBOutlet UILabel *answer2Label;
@property (weak, nonatomic) IBOutlet UILabel *answer3Label;
@property (weak, nonatomic) IBOutlet UILabel *answer4Label;
@property (weak, nonatomic) IBOutlet UIView *thresholdView;

- (IBAction)rightAnswer:(id)sender;
- (IBAction)wrongAnswer:(id)sender;
- (IBAction)resetAnswers:(id)sender;

@end
