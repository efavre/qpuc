//
//  EFSoundManager.m
//  qpuc
//
//  Created by Eric Favre on 19/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//

#import "EFSoundManager.h"
@import AVFoundation;

@interface EFSoundManager()
@property (nonatomic, strong) AVAudioPlayer *buzzerPlayer;
@property (nonatomic, strong) AVAudioPlayer *errorPlayer;
@end

@implementation EFSoundManager

+ (id)sharedManager {
    static EFSoundManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        [self initializeBuzzerSound];
        [self initializeErrorSound];
    }
    return self;
}

- (void)initializeBuzzerSound
{
    NSString *soundFilePath = [NSString stringWithFormat:@"%@/buzzer.m4a", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    NSError *error;
    self.buzzerPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
}

- (void)initializeErrorSound
{
    NSString *soundFilePath = [NSString stringWithFormat:@"%@/wrong.m4a", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    NSError *error;
    self.errorPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
}


- (void)playBuzzerSound
{
    [self.buzzerPlayer play];
}

- (void)playErrorSound
{
    [self.errorPlayer play];
}

@end
