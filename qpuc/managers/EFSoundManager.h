//
//  EFSoundManager.h
//  qpuc
//
//  Created by Eric Favre on 19/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EFSoundManager : NSObject

+ (id)sharedManager;
- (void)playBuzzerSound;
- (void)playErrorSound;

@end
