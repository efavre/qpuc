//
//  EFAppDelegate.h
//  qpuc
//
//  Created by Eric Favre on 11/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
