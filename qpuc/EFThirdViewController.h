//
//  EFThirdViewController.h
//  qpuc
//
//  Created by Eric Favre on 11/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFThirdViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *block1View;
@property (weak, nonatomic) IBOutlet UIView *block1BackgroundView;
@property (weak, nonatomic) IBOutlet UIView *block2View;
@property (weak, nonatomic) IBOutlet UIView *block2BackgroundView;
@property (weak, nonatomic) IBOutlet UIView *block3View;
@property (weak, nonatomic) IBOutlet UIView *block3BackgroundView;
@property (weak, nonatomic) IBOutlet UIView *block4View;
@property (weak, nonatomic) IBOutlet UIView *block4BackgroundView;

@property (weak, nonatomic) IBOutlet UIButton *player1Button;
@property (weak, nonatomic) IBOutlet UIButton *player2Button;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *switchHandsButton;
@property (weak, nonatomic) IBOutlet UIButton *startOverButton;

- (IBAction)startTimer:(id)sender;
- (IBAction)buzzerPushed:(id)sender;
- (IBAction)startOver:(id)sender;
- (IBAction)switchHand:(id)sender;
- (IBAction)wrongAnswer:(id)sender;

@end
