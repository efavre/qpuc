//
//  EFFirstViewController.m
//  qpuc
//
//  Created by Eric Favre on 11/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//

#import "EFFirstViewController.h"
#import "EFSoundManager.h"

@interface EFFirstViewController ()
@property (nonatomic) BOOL buttonLock;
@end

@implementation EFFirstViewController

- (void)viewDidLoad
{
    self.buttonLock = NO;
    [EFSoundManager sharedManager];
}

- (IBAction)buzzerPushed:(id)sender
{
    if (!self.buttonLock)
    {
        self.buttonLock = YES;
        UIButton *pushedButton = (UIButton *)sender;
        for (UIButton *button in @[self.user1Button, self.user2Button, self.user3Button, self.user4Button])
        {
            button.enabled = NO;
        }
        pushedButton.enabled = YES;
        pushedButton.selected = YES;
        [[EFSoundManager sharedManager] playBuzzerSound];
        [NSTimer scheduledTimerWithTimeInterval:5.0
                                     target:self
                                   selector:@selector(timerFinished:)
                                   userInfo:nil
                                    repeats:NO];
    }
}

- (void)timerFinished:(NSTimer *)timer
{
    [[EFSoundManager sharedManager] playErrorSound];
    for (UIButton *button in @[self.user1Button, self.user2Button, self.user3Button, self.user4Button])
    {
        button.selected = NO;
        button.hidden = NO;
        button.enabled = YES;
    }
    self.buttonLock = NO;
}



@end
