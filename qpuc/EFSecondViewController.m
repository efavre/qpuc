//
//  EFSecondViewController.m
//  qpuc
//
//  Created by Eric Favre on 11/06/2014.
//  Copyright (c) 2014 Eric Favre. All rights reserved.
//

#import "EFSecondViewController.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define THRESHOLD_OFFSET 173

@interface EFSecondViewController ()
@property(nonatomic) NSUInteger currentlyRightAnswersCount;
@end

@implementation EFSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self resetAnswers:nil];
}

- (IBAction)rightAnswer:(id)sender
{
    if (self.currentlyRightAnswersCount < 4)
    {
        [self moveThresholdUp];
        self.currentlyRightAnswersCount ++;
        UILabel *label = (UILabel *)[self.view viewWithTag:self.currentlyRightAnswersCount];
        label.backgroundColor = [UIColor blueColor];
    }
}

- (IBAction)wrongAnswer:(id)sender
{

    [self moveThresholdDown];
    self.currentlyRightAnswersCount = 0;
}

- (void)resetAnswers:(id)sender
{
    [self moveThresholdDown];
    self.currentlyRightAnswersCount = 0;
    UIColor *lightBlue = UIColorFromRGB(0x99CCFF);
    for (int tag = 1 ; tag <= 4 ; tag ++) {
        UILabel *label = (UILabel *)[self.view viewWithTag:tag];
        label.backgroundColor = lightBlue;
    }
}

- (void)moveThresholdUp
{
    CGRect newFrame = self.thresholdView.frame;
    newFrame.origin.y -= THRESHOLD_OFFSET;
    [UIView animateWithDuration:0.5 animations:^{
        self.thresholdView.frame = newFrame;
    }];
}

- (void)moveThresholdDown
{
    CGRect newFrame = self.thresholdView.frame;
    newFrame.origin.y += self.currentlyRightAnswersCount * THRESHOLD_OFFSET;
    [UIView animateWithDuration:0.5 animations:^{
        self.thresholdView.frame = newFrame;
    }];
}


@end
